import { Request, Response } from 'express';
import { container } from 'tsyringe';

import GetTitleBarcodeInfos from '@modules/boletos/services/GetTitleBarcodeInfos';

export default class BoletosController {
  public async show(request: Request, response: Response): Promise<Response> {
    const { barcode } = request.params;

    const getTitleBarcodeInfos = container.resolve(GetTitleBarcodeInfos);

    let barcodeInfos = {};

    if (barcode.length === 48) {
      console.log('titulo');
    } else {
      barcodeInfos = getTitleBarcodeInfos.execute({ barcode });
    }

    return response.json(barcodeInfos);
  }
}
