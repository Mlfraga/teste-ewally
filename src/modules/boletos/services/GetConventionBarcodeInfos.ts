interface IRequest {
  barcode: string;
}

interface IResponse {
  barcode: string;
  amount: string;
  expirationDate: Date;
}

class GetConventionBarcodeInfos {
  public async execute({ barcode }: IRequest): Promise<IResponse> {
    return {
      barcode,
      amount: '',
      expirationDate: new Date(),
    };
  }
}

export default GetConventionBarcodeInfos;
